class NegativeAmountException(Exception):
    pass

class NegativeBalanceException(Exception):
    pass

class NoBalanceException(Exception):
    pass

class SameUserException(Exception):
    pass