from rest_framework import serializers
from .models import Transfer

class TransferSerializer(serializers.ModelSerializer):

    user = serializers.HiddenField(
        default = serializers.CurrentUserDefault()
    )

    class Meta:
        model = Transfer
        fields = ['user','target_user','amount']