from django.db import models, transaction
from django.contrib.auth.models import User
from .exceptions import *
from user_balance.models import Balance

class Transfer(models.Model):

    user = models.ForeignKey('auth.User', related_name='transfer_user', on_delete=models.CASCADE,
                             blank=False, null=False)
    target_user = models.ForeignKey(User, related_name='transfer_target_user', on_delete=models.CASCADE,
                             blank=False, null=False)
    amount = models.DecimalField(decimal_places=2, max_digits=12)
    created_at = models.DateTimeField(auto_now_add=True)

    def transfer(self):

        target_balance = Balance.get_or_create_balance(self.target_user)
        target_balance.increase(self.amount)

        balance = self.user.balance.get()
        balance.decrease(-self.amount)

    def save(self, *args, **kwargs):

        if self.amount < 0:
            raise NegativeAmountException("Non negative amount allowed")

        if self.user.pk == self.target_user.pk:
            raise SameUserException("The user arget must be different to source user")

        balance = self.user.balance.get()
        if balance == None:
            raise NoBalanceException("The user has not balance")

        if (balance.amount - self.amount) < 0:
            raise NegativeBalanceException("The user has not enough amount in his balance")

        with transaction.atomic():
            super(Transfer, self).save(*args, **kwargs)
            self.transfer()




