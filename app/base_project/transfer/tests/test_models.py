import pytest
from mixer.backend.django import mixer
from django.contrib.auth.models import User
from transfer.exceptions import *
from rest_framework.test import APIClient
from rest_framework.test import APITestCase

pytestmark = pytest.mark.django_db

class TestTransfer(APITestCase):

    def setUp(self):

        self.client = APIClient()
        password = 'johnpassword'
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', password)
        self.balance = mixer.blend('user_balance.balance', user=self.user, amount=100)

        self.target_user = User.objects.create_user('jack', 'jack@thebeatles.com', password)
        self.target_balance = mixer.blend('user_balance.balance', user=self.target_user, amount=10)
        self.client.login(username=self.user.username, password=password)

    def test_transfer_create(self):

        obj = mixer.blend('transfer.transfer',user= self.user, target_user= self.target_user, amount=10)

        assert obj.pk == 1, 'Should create a transfer instance'

    def test_cannot_create_tranfer_greater_than_balance(self):

        with self.assertRaises(NegativeBalanceException):
            mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=101)

    def test_cannot_create_tranfer_with_same_source_and_target_user(self):

        with self.assertRaises(SameUserException):
            mixer.blend('transfer.transfer', user=self.user,target_user= self.user, amount=101)

    def test_can_create_several_tranfers(self):

        mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=10)
        mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=10)
        mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=10)
        mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=10)

    def test_cannot_create_transfer_without_target_user(self):

        with self.assertRaises(Exception):
            mixer.blend('transfer.transfer', user=self.user, target_user=None, amount=10)

    def test_creating_a_transfer_should_increase_target_user_balance_and_decrease_owned(self):

        amount = 10
        transfer = mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=amount)

        assert self.balance.amount == (transfer.user.balance.get().amount + amount),\
            'User balance decreased'
        assert self.target_balance.amount == (transfer.target_user.balance.get().amount - amount),\
            'Target balance increased'

    def test_cannot_create_negative_transfer(self):

        with self.assertRaises(NegativeAmountException):
            mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=-10)

    def test_can_create_transfer_with_decimal_amount(self):

        with self.assertRaises(Exception):
            mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount='dummy')

        obj = mixer.blend('transfer.transfer', user=self.user,target_user= self.target_user, amount=1.1)

        assert obj.pk == 1, 'Should create a balance instance'

    def test_can_create_a_transfer_with_a_target_user_without_a_balance(self):

        password = 'qwerty'
        target_user = User.objects.create_user('tim', 'carter@thebeatles.com', password)

        obj = mixer.blend('transfer.transfer', user=self.user, target_user=target_user, amount=10)

        assert obj.pk == 1, 'Should create a transfer instance'
