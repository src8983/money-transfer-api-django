import pytest
import json
from mixer.backend.django import mixer
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from rest_framework.test import APITestCase

pytestmark = pytest.mark.django_db

class TestTransferViewSet(APITestCase):

    def setUp(self):

        self.client = APIClient()
        self.base_url = reverse('transfer-list')

        password = 'johnpassword'
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', password)
        self.balance = mixer.blend('user_balance.balance', user=self.user,amount=100)

        self.target_user = User.objects.create_user('jack', 'jack@thebeatles.com', password)
        self.target_balance = mixer.blend('user_balance.balance', user=self.target_user, amount=10)
        self.client.login(username=self.user.username, password=password)

        self.transfer = mixer.blend('transfer.transfer', user=self.user, target_user=self.target_user, amount=10)

    def test_cannot_display_transfers_for_nonautenticated_user(self):

        self.client.logout()

        response = self.client.get(self.base_url)
        assert response.status_code == 403, 'Should be 403 Forbidden response type'

    def test_can_display_a_transfer(self):

        response = self.client.get(self.base_url)
        assert response.status_code == 200, 'Should be 200 response type'

        response_list = json.loads(response.content.decode())
        assert float(response_list[0]['amount']) == float(self.transfer.amount), \
            'Should be the same amount'

    def test_can_create_a_transfer(self):

        data = json.dumps({'target_user': self.target_user.pk,'amount':88})
        response = self.client.post(self.base_url,data,content_type='application/json')
        assert response.status_code == 201, 'Should be 201 response type'

        response_list = json.loads(response.content.decode())
        #print(response_list)
        #exit()
        assert float(response_list['amount']) == float(88), \
            'Should be the same amount'
