from django.db import models

class Balance(models.Model):

    user = models.ForeignKey('auth.User', related_name='balance', on_delete=models.CASCADE,
                             blank=False, null=False, unique=True)
    amount = models.DecimalField(decimal_places=2, max_digits=12)

    def increase(self,amount):
        self.amount = self.amount + amount
        self.save()

    def decrease(self,amount):
        self.amount = self.amount + amount
        self.save()

    @staticmethod
    def get_or_create_balance(user):
        try:
            balance = user.balance.get()
        except Balance.DoesNotExist:

            balance = Balance.objects.create(user=user,amount=0)

        return balance
