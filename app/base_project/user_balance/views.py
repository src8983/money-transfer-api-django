from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from .serializers import BalanceSerializer
from base_project.permissions import IsOwner
from .models import Balance

class BalanceViewSet(ModelViewSet):

    queryset = Balance.objects.all()
    serializer_class = BalanceSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        user = self.request.user
        return self.queryset.filter(user=user)