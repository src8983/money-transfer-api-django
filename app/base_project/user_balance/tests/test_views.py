import pytest
import json
from mixer.backend.django import mixer
from django.urls import reverse
from rest_framework.test import APIClient
from django.contrib.auth.models import User
from rest_framework.test import APITestCase

pytestmark = pytest.mark.django_db

class TestBalanceViewSet(APITestCase):

    def setUp(self):

        self.client = APIClient()
        self.base_url = reverse('balance-list')

        password = 'johnpassword'
        self.user = User.objects.create_user('john', 'lennon@thebeatles.com', password)
        self.balance = mixer.blend('user_balance.balance', user=self.user)
        self.client.login(username=self.user.username, password=password)

    def test_cannot_display_balance_for_nonautenticated_user(self):

        self.client.logout()

        response = self.client.get(self.base_url)
        assert response.status_code == 403, 'Should be 403 Forbidden response type'

    def test_can_display_a_user_balance(self):

        response = self.client.get(self.base_url)
        assert response.status_code == 200, 'Should be 200 response type'

        response_list = json.loads(response.content.decode())
        assert float(response_list[0]['amount'] ) == float(self.balance.amount), \
            'Should be the same amount'

    def test_cannot_display_balance_of_other_user(self):

        self.client.logout()

        password = 'jackpassword'
        other_user = User.objects.create_user('jack', 'jack@thebeatles.com', password)
        balance_other_user = mixer.blend('user_balance.balance', user=other_user)
        self.client.login(username=other_user.username, password=password)

        print(self.client.session['_auth_user_id'])

        response = self.client.get(self.base_url)
        assert response.status_code == 200, 'Should be 200 response type'

        response_list = json.loads(response.content.decode())
        assert float(response_list[0]['amount']) == float(balance_other_user.amount), \
            'Should be the same amount'


