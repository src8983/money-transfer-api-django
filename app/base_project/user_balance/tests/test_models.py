import pytest
from mixer.backend.django import mixer
from user_balance.models import *
from django.contrib.auth.models import User
from rest_framework.test import APITestCase

pytestmark = pytest.mark.django_db

class TestBalance(APITestCase):

    def test_balance_create(self):

        obj = mixer.blend('user_balance.balance')

        assert obj.pk == 1, 'Should create a balance instance'

    def test_balance_create_multiple(self):

        mixer.blend('user_balance.balance')
        mixer.blend('user_balance.balance')
        mixer.blend('user_balance.balance')
        mixer.blend('user_balance.balance')

        assert Balance.objects.all().count() == 4, 'Should create 4 balances instances'

    def test_can_create_balance_with_decimal_amount(self):

        with self.assertRaises(Exception):
            mixer.blend('user_balance.balance', amount='dummy')

        obj = mixer.blend('user_balance.balance', amount=1.1)

        assert obj.pk == 1, 'Should create a balance instance'

    def test_cannot_create_a_balance_without_amount(self):

        with self.assertRaises(Exception):
            mixer.blend('user_balance.balance', amount=None)

    def test_cannot_create_a_balance_without_a_user(self):

        with self.assertRaises(Exception):
            mixer.blend('user_balance.balance', user=None)

    def test_cannot_create_duplicated_balance_from_same_user(self):

        john = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
        balance1 = mixer.blend('user_balance.balance', user=john)

        assert balance1.pk == 1, 'Should create a balance instance'

        with self.assertRaises(Exception):
            mixer.blend('user_balance.balance', user=john)
