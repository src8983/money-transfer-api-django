from django.apps import AppConfig


class UserBalanceConfig(AppConfig):
    name = 'user_balance'
