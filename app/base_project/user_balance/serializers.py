from rest_framework import serializers
from .models import Balance

class BalanceSerializer(serializers.HyperlinkedModelSerializer):

    user = serializers.HiddenField(
        default = serializers.CurrentUserDefault()
    )

    class Meta:
        model = Balance
        fields = ['user','amount']