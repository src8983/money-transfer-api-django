
Run in a Docker:

docker-compose build
docker-compose up

Run the test:

docker exec -it {$name_docker} /bin/bash
cd base_project && pytest

The list of functionalities are:

- 1- A user can login. http://localhost:8000/api-auth/login/
- 2- Script for create foo users: cd /app/base_project && cat create_user.py | python manage.py shell
- 3- A user can create a balance, just once. http://localhost:8000/balance/
- 4- A user can display his own balance. http://localhost:8000/balance/

The projects has:

- Two apps transfer and user_balance.

- user_balance: Has a Balance model, that is unique for each user.
    
- transfer: Has a Transfer model, that allows to transfer an amount between two different Balances.
That model has some business validations before save.
Each save in the model change the balances, that is implemented in a transaction for making it atomic.
 